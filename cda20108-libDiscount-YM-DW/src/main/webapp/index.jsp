<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Libdiscount Connexion</title>
<link rel="stylesheet" href="CSS/css_connexion.css">
</head>
<body>
  <!--  HEADER -------------------------------->
  <header class="bar">
    <!-- <strong  id="Libdiscount"></strong> -->
    <img src="assets/logo2.png" alt="logo" id="logo">
    <div class="menu">
      <ul>
        <li><a href="#" id="menuPage">Connexion</a></li>
        <li><a href="FormInscription">Inscription</a></li>
      </ul>
    </div>
  </header>
  <!--  FORMULAIRE ---------------------------->
  <div class="" id="containeformulaire">
    <form action="ConnexionUtilisateur" method="post">

        <h1>CONNEXION</h1>

  			<input type="text" placeholder="Login" name="login" required class="champ"/>


  			<input type="password" placeholder="Mot de passe" name="pwd" required class="champ"/>


        <button type="submit" id="buttonSubmit">Valider</button>

        <label id="souvenir" ><input type="checkbox"  name="remember" /> Se souvenir de moi</label>

        <div class="" id="redirection">
          <p class="texte">Nouveau ?</p>
          <p class="texte" id="textRedirect"><a href="FormInscription" id="hoverRedirect">Inscrivez-vous ici</a></p>
        </div>

      </form>
      <a href="AfficherMenuAdmin" id="hoverRedirect">admin</a>
  </div>
  <!--  FOOTER ----------------------------->
  <footer></footer>
  </body>
</html>
