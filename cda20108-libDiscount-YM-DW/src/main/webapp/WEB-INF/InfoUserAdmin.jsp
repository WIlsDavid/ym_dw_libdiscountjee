<%@page import="fr.afpa.beans.Annonce"%>
<%@page import="fr.afpa.beans.Utilisateur"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Info User page admin</title>
	<link rel="stylesheet" href="CSS/css_infoUserAdmin.css">
</head>
<body>

		<%
			Utilisateur user = (Utilisateur)request.getAttribute("user");
		%>
<!--  HEADER -------------------------------->
  <header class="bar">
    <!-- <strong  id="Libdiscount"></strong> -->
    <img src="assets/logo2.png" alt="logo" id="logo">
    <div class="menu">
      <ul>
        <li><a href="#" id="menuPage">Accueil</a></li>
        <li><a href="#">Annonces</a></li>
        <li><a href="#">Compte</a></li>
        <li><a href="#"> <img src="assets/logout.png" alt="" id="logout"> </a></li>
      </ul>
    </div>
  </header>
  <!--  FORMULAIRE ---------------------------->
  <div class="" id="containeformulaire">
     <form action="UpdateUser" method="post">



        <h1>Modifier son compte</h1>
       	<input type="hidden" value="<c:out value="${user.idUser}" />" name="id"/>
        <div id="case">


      		<input type="text" name="nom_librairie" value="<c:out value="${user.nomLib}" />" placeholder="Nom de votre librairie" class="champ" required/>
      		<input type="text" name="nom_user" value="<c:out value="${user.nom}" />" placeholder="Nom>" class="champ" required/>
        	<input type="text" name="prenom_user" class="champ" value="<c:out value="${user.prenom}"/>" placeholder="Prenom" required/>
         	<input type="email" name="email" class="champ" value="<c:out value="${user.mail}"/>" placeholder="Email" required/>
         	<input type="text" name="num_tel" class="champ" value="<c:out value="${user.tel}"/>" placeholder="numero de telephone" required/>

          <input type="hidden" name="idCompte" value="<c:out value="${user.getCompte().idCompte}"/>" required/>
          
          <input type="text" name="login" class="champ" value="<c:out value="${user.getCompte().getLogin()}"/>" placeholder="Login" required/>
          <input type="password" name="pwd" class="champ" value="<c:out value="${user.getCompte().mdp}"/>" placeholder="Mot de passe" required/>
          <input type="password" name="pwd2" class="champ" placeholder="Confirmer mot de passe" required/>

          <input type="hidden" name="idAdresse" value="<c:out value="${user.getAdresse().idAdresse}"/>"/>

          <input type="number" name="num_voie" class="champ" value="<c:out value="${user.getAdresse().numRue}"/>" placeholder="Numero de rue" min="0" max="999" required/>
          <input type="text" name="nom_voie" class="champ" value="<c:out value="${user.getAdresse().nomRue}"/>" placeholder="Nom de rue" required/>
          <input type="number" name="cpostal" class="champ" value="<c:out value="${user.getAdresse().codeP}"/>" placeholder="Code postale" min="10000" max="99999" required/>
          <input type="text" name="ville" class="champ" value="<c:out value="${user.getAdresse().ville}"/>" placeholder="Ville" required/>
      </div>
        <button type="submit" id="boutonValide"><img src="assets/icones_modif.png" alt=""></button>

      </form>
  </div>
  <!--  MES ANNONCES  ----------------------------->

    <c:forEach items="${ListeAnnonce}" var="annonce">
	  		<div id="annonce">
	  			<div id="info">
	  				<h3><c:out value="${annonce.titreAnnonce}"/></h3>
	  				<div>
	  					<fmt:parseDate value="${annonce.dateAnnonce}" type="date" pattern="yyyy-MM-dd" var="parseDateAnnonce"/>
	  					<p> publie le : <fmt:formatDate value="${parseDateAnnonce}" type="date" pattern="dd/MM/yyyy" dateStyle="short"/></p>
	  					<p>Titre   : <c:out value="${annonce.titreLivre}"/></p>
						<p>ISBN    : <c:out value="${annonce.isbn}"/></p>
						<p>De      : <c:out value="${annonce.maisonEdition}"/></p>
						<p>Niveau  : <c:out value="${annonce.niveauScolaire}"/></p>
	  				</div>
	  				<div>
						<p>Stock   : <c:out value="${annonce.quantite}"/></p>
						<fmt:parseDate value="${annonce.dateEdition}" type="date" pattern="yyyy-MM-dd" var="parseDateEdition"/>
	  					<p> Edition : <fmt:formatDate value="${parseDateEdition}" type="date" pattern="dd/MM/yyyy" dateStyle="short"/></p>
						<p>Remise  : <c:out value="${annonce.remise}"/></p>
						<p>Ville  : <c:out value="${annonce.getUser().getAdresse().ville}"/></p>
						<c:set var="prixtotal" value="${ annonce.quantite * annonce.prixUni * (1 -(annonce.remise / 100))}"/>
						<div id="prixtotal">
							<fmt:formatNumber value="${prixtotal}" type="currency" currencySymbol="€" />
						</div>
					</div>
					<div id="photos">
						<c:if test="${annonce.image1 != null}">
							<img src="<c:url value="/annonce/${annonce.image1}"/>">
							<c:if test="${annonce.image2 != null}">
								<img src="<c:url value="/annonce/${annonce.image2}"/>">
								<c:if test="${annonce.image3 != null}">
									<img src="<c:url value="/annonce/${annonce.image3}"/>"/>
								</c:if>
							</c:if>
						</c:if>
						<c:if test="${annonce.image1 eq null}">
							<img src="assets/book.jpg" alt="">
						</c:if>
					</div>		
	  			</div>
	  			<div id="boutonsAnnonce">
	  				<c:url value="/DeleteAnnonce" var="lienDelete">
	  					<c:param name="iduser" value="${user.idUser}"/>
	  					<c:param name="idannonce" value="${annonce.idAnnonce}"/>
	  				</c:url>		
	  				<c:url value="/UpdateAnnonce" var="lienUpdate">
	  					<c:param name="iduser" value="${user.idUser}"/>
	  					<c:param name="idannonce" value="${annonce.idAnnonce}"/>
	  				</c:url>		
	      			<button type="button" name="button"><a href="${lienDelete}"><img src="assets/icones_supp.png" alt="" class="boutonOption"></a></button>
	       			<button type="button" name="button"><a href="${lienUpdate}"><img src="assets/icones_modif.png" alt="" class="boutonOption"></a></button>
	     		</div>
	  		</div>
	  	</c:forEach>
  <!--  FOOTER ----------------------------->
  <footer></footer>

</body>
</html>