<%@page import="fr.afpa.beans.Utilisateur"%>
<%@page import="fr.afpa.beans.Annonce"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Libdiscount - Liste Annonces</title>
<link rel="stylesheet" href="CSS/css_listeAnnonce.css">
</head>
<body>
	<!-- Attributes  ListeAnnonce  user -->
		
			<header class="bar">
		    <!-- <strong  id="Libdiscount"></strong> -->
		    <img src="assets/logo1_blanc.png" alt="logo" id="logo">
		
		
		    <div class="menu">
		      <ul>
		        <li><a href="#" id="menuPage">Accueil</a></li>
		        <li><a href="ListeAnnonces?id=<c:out value="${user.idUser}"/>">Annonces</a></li>
	        	<li><a href="FormModifUtilisateur?id=<c:out value="${user.idUser}"/>&modif=mesinfos">Compte</a></li>
		        <li><a href="index.jsp"> <img src="assets/logout.png" alt="" id="logout"> </a></li>
		      </ul>
		    </div>
		  </header>
		  <label for="recherche">Recherche : </label>
		  <input type="search" name="recherche" value="" placeholder="Recherche par mot-clé" />
  
		<!--  MES ANNONCES  ----------------------------->
		
		<c:forEach items="${ListeAnnonce}" var="annonce">
			<div id="annonce">
				<div id="info">
					<h3><c:out value="${annonce.titreAnnonce}"/></h3>
					<fmt:parseDate value="${annonce.dateAnnonce}" type="date"
						pattern="yyyy-MM-dd" var="parseDateAnnonce" />
					<p>
						publie le :
						<fmt:formatDate value="${parseDateAnnonce}" type="date"
							pattern="dd/MM/yyyy" dateStyle="short" />
					</p>
					<p>Titre   : <c:out value="${annonce.titreLivre}"/></p>
					<p>ISBN    : <c:out value="${annonce.isbn}"/></p>
					<p>De      : <c:out value="${annonce.maisonEdition}"/></p>
					<p>Niveau  : <c:out value="${annonce.niveauScolaire}"/></p>
				
					<div>
						<p>Stock   : <c:out value="${annonce.quantite}"/></p>
						<fmt:parseDate value="${annonce.dateEdition}" type="date"
						pattern="yyyy-MM-dd" var="parseDateEdition" />
						<p>
							Edition : <fmt:formatDate value="${parseDateEdition}" type="date"
							pattern="dd/MM/yyyy" dateStyle="short" />
						</p>
						<p>Remise  : <c:out value="${annonce.remise}"/></p>
					</div>
					<c:set var="prixtotal" value="${ annonce.quantite * annonce.prixUni * (1 -(annonce.remise / 100))}" />
				<div id="prixtotal">
					<fmt:formatNumber value="${prixtotal}" type="currency"
						currencySymbol="€" />
				</div>
				</div>
				
				<div id="photos">
					<c:if test="${annonce.image1 != null}">
						<img src="<c:url value="/annonce/${annonce.image1}"/>">
						<c:if test="${annonce.image2 != null}">
							<img src="<c:url value="/annonce/${annonce.image2}"/>">
							<c:if test="${annonce.image3 != null}">
								<img src="<c:url value="/annonce/${annonce.image3}"/>" />
							</c:if>
						</c:if>
					</c:if>
					<c:if test="${annonce.image1 eq null}">
						<img src="assets/book.jpg" alt="">
					</c:if>
				</div>
			</div>
		</c:forEach>
</body>
</html>