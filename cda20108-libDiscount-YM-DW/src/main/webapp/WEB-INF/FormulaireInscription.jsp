<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>Libdiscount Inscription</title>
    <link rel="stylesheet" href="CSS/css_inscription.css">
</head>
<body>
  <!------------------------ HEADER -------------------------------->
  <header class="bar">
    <!-- <strong  id="Libdiscount"></strong> -->
    <img src="assets/logo2.png" alt="logo" id="logo">
    <div class="menu">
      <ul>
        <li><a href="index.jsp" id="menuPage" class="menuLien">Connexion</a></li>
        <li><a href="#" class="menuLien">Inscription</a></li>
      </ul>
    </div>
  </header>
  <!----------------------- FORMULAIRE ---------------------------->
  <div class="" id="containeformulaire">
    <form action="AddUser" method="post">

  			<input type="text" placeholder="Nom de votre Librairie" name="nom_librairie" required class="champ"/>

  			<input type="text" placeholder="nom" name="nom_user" required class="champ"/>


  			<input type="text" placeholder="Prenom" name="prenom_user" required class="champ"/>


  			<input type="email" placeholder="Email" name="email" required class="champ"/>


  			<input type="text" placeholder="Numero de telephone" name="num_tel" required class="champ"/>


  			<input type="text" placeholder="Login" name="login" required class="champ"/>


  			<input type="password" placeholder="Mot de passe" name="pwd" required class="champ"/>


  			<input type="password" placeholder="Confirmer votre mot de passe" name="pwd2" required class="champ" />


  			<input type="text" placeholder="Numero de rue" name="num_voie" required class="champ"/>


  			<input type="text" placeholder="Nom de rue" name="nom_voie" required class="champ"/>


  			<input type="text" placeholder="Code postal" name="cpostal" required class="champ"/>


  			<input type="text" placeholder="Ville" name="ville" required class="champ"/>

        <button type="submit" id="buttonSubmit">Valider</button>



        <div class="" id="redirection">
          <p class="texte">Deja un compte ?</p>
          <p class="texte" id="textRedirect"><a href="index.jsp" id="hoverRedirect">Connectez-vous ici</a></p>
        </div>
      </form>

  </div>
  <!-- --------------------- FOOTER ----------------------------->
  <footer></footer>
  </body>
</html>