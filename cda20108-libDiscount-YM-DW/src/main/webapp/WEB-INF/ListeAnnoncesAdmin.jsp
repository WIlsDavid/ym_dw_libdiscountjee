<%@page import="fr.afpa.beans.Annonce"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Libdiscount Admin Annonces</title>
<link rel="stylesheet" href="CSS/css_listeCompteAdmin.css">
</head>
<body>
<%
		List<Annonce> list = (List<Annonce>) request.getAttribute("ListeAnnonce");
		
		
	%>
<!--  HEADER -------------------------------->
  <header class="bar">
    <!-- <strong  id="Libdiscount"></strong> -->
    <img src="assets/logo1_blanc.png" alt="logo" id="logo">


    <div class="menu">
      <ul>
        <li><a href="#">Annonces</a></li>
        <li><a href="#">Comptes</a></li>
        <li><a href="#"> <img src="assets/logout.png" alt="" id="logout"> </a></li>
      </ul>
    </div>
  </header>
  
   <div class="" id="buttonChange">
    <button type="button" name="button" id="assets/buttonChangementFenetre">
		<a href="AfficherMenuAdmin">
          <p>Acceder aux comptes</p>
		</a>
    </button>
  </div>


  <div class="" id="divRecherche">
	  <form action="">
	    <label for="recherche">Recherche : </label>
	    <input type="search" name="recherche" value="" placeholder="Recherche par mot-clé">
	    <!-- <button>valider</button> -->
	   </form>
  </div>
	<c:forEach items="${ListeAnnonce}" var="annonce">
	  		<div id="annonce">
	  			<div id="info">
	  				<h3><c:out value="${annonce.titreAnnonce}"/></h3>
	  				<div>
	  					<fmt:parseDate value="${annonce.dateAnnonce}" type="date" pattern="yyyy-MM-dd" var="parseDateAnnonce"/>
	  					<p> publie le : <fmt:formatDate value="${parseDateAnnonce}" type="date" pattern="dd/MM/yyyy" dateStyle="short"/></p>
	  					<p>Titre   : <c:out value="${annonce.titreLivre}"/></p>
						<p>ISBN    : <c:out value="${annonce.isbn}"/></p>
						<p>De      : <c:out value="${annonce.maisonEdition}"/></p>
						<p>Niveau  : <c:out value="${annonce.niveauScolaire}"/></p>
	  				</div>
	  				<div>
						<p>Stock   : <c:out value="${annonce.quantite}"/></p>
						<fmt:parseDate value="${annonce.dateEdition}" type="date" pattern="yyyy-MM-dd" var="parseDateEdition"/>
	  					<p> Edition : <fmt:formatDate value="${parseDateEdition}" type="date" pattern="dd/MM/yyyy" dateStyle="short"/></p>
						<p>Remise  : <c:out value="${annonce.remise}"/></p>
						<p>Ville  : <c:out value="${annonce.getUser().getAdresse().ville}"/></p>
						<c:set var="prixtotal" value="${ annonce.quantite * annonce.prixUni * (1 -(annonce.remise / 100))}"/>
						<div id="prixtotal">
							<fmt:formatNumber value="${prixtotal}" type="currency" currencySymbol="€" />
						</div>
					</div>
					<div id="photos">
						<c:if test="${annonce.image1 != null}">
							<img src="<c:url value="/annonce/${annonce.image1}"/>">
							<c:if test="${annonce.image2 != null}">
								<img src="<c:url value="/annonce/${annonce.image2}"/>">
								<c:if test="${annonce.image3 != null}">
									<img src="<c:url value="/annonce/${annonce.image3}"/>"/>
								</c:if>
							</c:if>
						</c:if>
						<c:if test="${annonce.image1 eq null}">
							<img src="assets/book.jpg" alt="">
						</c:if>
					</div>		
	  			</div>
	  			<div id="boutonsAnnonce">
	  				<c:url value="/DeleteAnnonce" var="lienDelete">
	  					<c:param name="iduser" value="${user.idUser}"/>
	  					<c:param name="idannonce" value="${annonce.idAnnonce}"/>
	  				</c:url>		
	  				<c:url value="/UpdateAnnonce" var="lienUpdate">
	  					<c:param name="iduser" value="${user.idUser}"/>
	  					<c:param name="idannonce" value="${annonce.idAnnonce}"/>
	  				</c:url>		
	      			<button type="button" name="button"><a href="${lienDelete}"><img src="assets/icones_supp.png" alt="" class="boutonOption"></a></button>
	       			<button type="button" name="button"><a href="${lienUpdate}"><img src="assets/icones_modif.png" alt="" class="boutonOption"></a></button>
	     		</div>
	  		</div>
	  	</c:forEach>
</body>
</html>