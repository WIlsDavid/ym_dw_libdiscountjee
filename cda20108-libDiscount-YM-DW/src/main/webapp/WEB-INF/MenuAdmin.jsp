<%@page import="fr.afpa.beans.Utilisateur"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Menu Admin</title>
<link rel="stylesheet" href="CSS/css_listeCompteAdmin.css">
</head>
<body>
	
<!--  HEADER -------------------------------->
  <header class="bar">
    <!-- <strong  id="Libdiscount"></strong> -->
    <img src="assets/logo1_blanc.png" alt="logo" id="logo">


    <div class="menu">
      <ul>
        <li><a href="#">Annonces</a></li>
        <li><a href="#">Comptes</a></li>
        <li><a href="#"> <img src="assets/logout.png" alt="" id="logout"> </a></li>
      </ul>
    </div>
  </header>

  <div class="" id="buttonChange">
    <button type="button" name="button" id="assets/buttonChangementFenetre">
		<a href="ListesAnnoncesAdmin">
          <p>Acceder aux annonces</p>
		</a>
    </button>
  </div>


  <div class="" id="divRecherche">
	  <form action="">
	    <label for="recherche">Recherche : </label>
	    <input type="search" name="recherche" value="" placeholder="Recherche par mot-cl�">
	    <!-- <button>valider</button> -->
	   </form>
  </div>


<!--  MES ANNONCES  ----------------------------->
<c:forEach items="${listUser}" var="user">
	<div id="annonce">
		<div id="info">
			<h1><c:out value="${user.nomLib}"/></h1>
			<h3><c:out value="${user.nom}"/> <c:out value="${user.prenom}"/></h3>
			<p><c:out value="${user.mail}"/></p>
			<p><c:out value="${user.tel}"/></p>
			<p><c:out value="${user.adresse.numRue}"/> <c:out value="${user.adresse.nomRue}"/> <c:out value="${user.adresse.codeP}"/>, <c:out value="${user.adresse.ville}"/></p>
			<p>Login : <c:out value="${user.compte.login}"/></p>
		</div>
		<div id="boutonsAnnonce">
		      
		      <button type="button" name="button"><a href="UtilisateurAdmin?id=${user.idUser}"><img src="assets/icones_modif.png" alt="" class="boutonOption"></a></button>

		</div>
	</div>

</c:forEach>


</body>
</html>