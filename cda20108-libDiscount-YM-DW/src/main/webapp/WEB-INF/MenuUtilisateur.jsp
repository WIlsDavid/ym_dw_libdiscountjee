<%@page import="java.util.ArrayList"%>
<%@page import="fr.afpa.beans.Annonce"%>
<%@page import="fr.afpa.beans.Utilisateur"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>LibDiscount - Menu Utilisateur</title>
<link rel="stylesheet" href="CSS/css_interfaceUser.css">
</head>
<body>
	<!-- Attributes : user  listAnnonce -->
	<!--  HEADER -------------------------------->
	<header class="bar">
		<!-- <strong  id="Libdiscount"></strong> -->
		<img src="assets/logo1_blanc.png" alt="logo" id="logo">


		<div class="menu">
			<ul>
				<li><a href="#" id="menuPage">Accueil</a></li>
				<li><a href="ListeAnnonces?id=<c:out value="${user.idUser}"/>">Annonces</a></li>
				<li><a
					href="FormModifUtilisateur?id=<c:out value="${user.idUser}"/>&modif=mesinfos">Compte</a></li>
				<li><a href="index.jsp"> <img src="assets/logout.png"
						alt="" id="logout">
				</a></li>
			</ul>
		</div>
	</header>


	<!--  FORMULAIRE ---------------------------->
	<div class="" id="divNavigation">
		<h1 id="titreInterface">INTERFACE CLIENT</h1>
		<h1>
			Bonjour
			<c:out value="${user.prenom}" />
			<c:out value="${user.nom}" />
		</h1>
		<h3>Que voulez vous faire ?</h3>

		<div id="navigation">
			<div class="section" id="add_annonce">
				<button type="button" name="button" class="boutonInterface">
					<a href="AddAnnonce?id=<c:out value="${user.idUser}"/>"><img
						src="assets/icone_ajout.png" alt="" class="icone"></a>
				</button>

				<p>ajouter une annonce</p>

			</div>
			<div class="section" id="voir_annonces">
				<button type="button" name="button" class="boutonInterface">
					<a href="ListeAnnonces?id=<c:out value="${user.idUser}"/>"><img
						src="assets/icone_list.png" alt="" class="icone"></a>
				</button>

				<p>Consulter les annonces</p>

			</div>
			<div class="section" id="voir_comptePerso">
				<button type="button" name="button" class="boutonInterface">
					<a
						href="FormModifUtilisateur?id=<c:out value="${user.idUser}"/>&modif=mesinfos"><img
						src="assets/icones_user.png" alt="" class="icone"></a>
				</button>

				<p>Consulter son compte</p>

			</div>
			<div class="section" id="voir_ses_annonces">
				<button type="button" name="button" class="boutonInterface">
					<a href="#"><img src="assets/icones_Annonces_perso.png" alt=""
						class="icone"></a>
				</button>

				<p>Consulter ses annonces</p>

			</div>
		</div>

	</div>
	<!--  MES ANNONCES BANDEROLE ----------------------------->
	<div class="" id="banderole">
		<div class="" id="titreCentrer">
			<h1 id="titreBanderole">MES ANNONCES</h1>
		</div>
	</div>
	<c:forEach items="${listAnnonce}" var="annonce">
		<div id="annonce">
			<div id="info">
				<h3>
					<c:out value="${annonce.titreAnnonce}" />
				</h3>
				<div>
					<fmt:parseDate value="${annonce.dateAnnonce}" type="date"
						pattern="yyyy-MM-dd" var="parseDateAnnonce" />
					<p>
						publie le :
						<fmt:formatDate value="${parseDateAnnonce}" type="date"
							pattern="dd/MM/yyyy" dateStyle="short" />
					</p>
					<p>
						Titre :
						<c:out value="${annonce.titreLivre}" />
					</p>
					<p>
						ISBN :
						<c:out value="${annonce.isbn}" />
					</p>
					<p>
						De :
						<c:out value="${annonce.maisonEdition}" />
					</p>
					<p>
						Niveau :
						<c:out value="${annonce.niveauScolaire}" />
					</p>
				</div>
				<div>
					<p>
						Stock :
						<c:out value="${annonce.quantite}" />
					</p>
					<fmt:parseDate value="${annonce.dateEdition}" type="date"
						pattern="yyyy-MM-dd" var="parseDateEdition" />
					<p>
						Edition :
						<fmt:formatDate value="${parseDateEdition}" type="date"
							pattern="dd/MM/yyyy" dateStyle="short" />
					</p>
					<p>
						Remise :
						<c:out value="${annonce.remise}" />
					</p>
					<p>
						Ville :
						<c:out value="${annonce.getUser().getAdresse().ville}" />
					</p>
					<c:set var="prixtotal"
						value="${ annonce.quantite * annonce.prixUni * (1 -(annonce.remise / 100))}" />

				</div>
				<div id="prixtotal">
					<fmt:formatNumber value="${prixtotal}" type="currency"
						currencySymbol="€" />
				</div>
				
			</div>
			<div id="photos">
					<c:if test="${annonce.image1 != null}">
						<img src="<c:url value="/annonce/${annonce.image1}"/>">
						<c:if test="${annonce.image2 != null}">
							<img src="<c:url value="/annonce/${annonce.image2}"/>">
							<c:if test="${annonce.image3 != null}">
								<img src="<c:url value="/annonce/${annonce.image3}"/>" />
							</c:if>
						</c:if>
					</c:if>
					<c:if test="${annonce.image1 eq null}">
						<img src="assets/book.jpg" alt="">
					</c:if>
				</div>
			<div id="boutonsAnnonce">
				<c:url value="/DeleteAnnonce" var="lienDelete">
					<c:param name="iduser" value="${user.idUser}" />
					<c:param name="idannonce" value="${annonce.idAnnonce}" />
				</c:url>
				<c:url value="/UpdateAnnonce" var="lienUpdate">
					<c:param name="iduser" value="${user.idUser}" />
					<c:param name="idannonce" value="${annonce.idAnnonce}" />
				</c:url>
				<button type="button" name="button">
					<a href="${lienDelete}"><img src="assets/icones_supp.png"
						alt="" class="boutonOption"></a>
				</button>
				<button type="button" name="button">
					<a href="${lienUpdate}"><img src="assets/icones_modif.png"
						alt="" class="boutonOption"></a>
				</button>
			</div>
		</div>
	</c:forEach>
</body>
</html>