<%@page import="fr.afpa.beans.Utilisateur"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Modifier son compte</title>
    <link rel="stylesheet" href="CSS/css_modifierCompte.css">
</head>
<body>

<!-- Attributes : user -->

<!--  HEADER -------------------------------->
  <header class="bar">
    <!-- <strong  id="Libdiscount"></strong> -->
    <img src="assets/logo2.png" alt="logo" id="logo">
    <div class="menu">
      <ul>
        <li><a href="HomePage.html" id="menuPage">Accueil</a></li>
        <li><a href="#">Annonces</a></li>
        <li><a href="#">Compte</a></li>
        <li><a href="index.jsp"> <img src="assets/logout.png" alt="" id="logout"> </a></li>
      </ul>
    </div>
  </header>

      <!--  FORMULAIRE ---------------------------->
  <div class="" id="containeformulaire">
    <form action="UpdateUser" method="post">



        <h1>Modifier son compte</h1>
       	<input type="hidden" value="<c:out value="${user.idUser}" />" name="id"/>
        <div id="case">


      		<input type="text" name="nom_librairie" value="<c:out value="${user.nomLib}" />" placeholder="Nom de votre librairie" class="champ" required/>
      		<input type="text" name="nom_user" value="<c:out value="${user.nom}" />" placeholder="Nom>" class="champ" required/>
        	<input type="text" name="prenom_user" class="champ" value="<c:out value="${user.prenom}"/>" placeholder="Prenom" required/>
         	<input type="email" name="email" class="champ" value="<c:out value="${user.mail}"/>" placeholder="Email" required/>
         	<input type="text" name="num_tel" class="champ" value="<c:out value="${user.tel}"/>" placeholder="numero de telephone" required/>

          <input type="hidden" name="idCompte" value="<c:out value="${user.getCompte().idCompte}"/>" required/>
          
          <input type="text" name="login" class="champ" value="<c:out value="${user.getCompte().getLogin()}"/>" placeholder="Login" required/>
          <input type="password" name="pwd" class="champ" value="<c:out value="${user.getCompte().mdp}"/>" placeholder="Mot de passe" required/>
          <input type="password" name="pwd2" class="champ" placeholder="Confirmer mot de passe" required/>

          <input type="hidden" name="idAdresse" value="<c:out value="${user.getAdresse().idAdresse}"/>"/>

          <input type="number" name="num_voie" class="champ" value="<c:out value="${user.getAdresse().numRue}"/>" placeholder="Numero de rue" min="0" max="999" required/>
          <input type="text" name="nom_voie" class="champ" value="<c:out value="${user.getAdresse().nomRue}"/>" placeholder="Nom de rue" required/>
          <input type="number" name="cpostal" class="champ" value="<c:out value="${user.getAdresse().codeP}"/>" placeholder="Code postale" min="10000" max="99999" required/>
          <input type="text" name="ville" class="champ" value="<c:out value="${user.getAdresse().ville}"/>" placeholder="Ville" required/>
      </div>
        <button type="submit" id="boutonValide"><img src="assets/icones_modif.png" alt=""></button>

      </form>
  </div>
</body>
</html>