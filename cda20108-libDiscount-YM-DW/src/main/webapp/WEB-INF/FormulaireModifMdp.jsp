<%@page import="fr.afpa.beans.Utilisateur"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<!-- attributs : user -->

<form action="UpdatePwd" method="post">

			<input type="hidden" name="id" value="<c:out value="${user.getCompte().idCompte}"/>"/>
			
			<label for="login"><b>login</b></label>
			<input type="text" value="<c:out value="${user.getCompte().login}"/>" name="login" required/>

            <label for="pwd"><b>mot de passe</b></label>
			<input type="password" value="<c:out value="${user.getCompte().mdp}"/>" name="pwd" required/>

            <label for="pwd2"><b>confirmer votre mot de passe</b></label>
			<input type="password" placeholder="confirmer votre mot de passe" name="pwd2" required/>
			
            <button type="submit">Valider</button>
    </form>
</body>
</html>