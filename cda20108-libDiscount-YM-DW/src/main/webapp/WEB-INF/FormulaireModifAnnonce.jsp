<%@page import="fr.afpa.beans.Annonce"%>
<%@page import="fr.afpa.beans.Utilisateur"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Modifier l'annonce</title>
<link rel="stylesheet" href="CSS/css_ajoutAnnonce.css">
</head>
<body>

<!-- attributs : user  idUser annonce -->

  <!--  HEADER -------------------------------->
  <header class="bar">
    <!-- <strong  id="Libdiscount"></strong> -->
    <img src="assets/logo2.png" alt="logo" id="logo">
    <div class="menu">
      <ul>
        <li><a href="HomePage.html" id="menuPage">Accueil</a></li>
        <li><a href="ListeAnnonces?id=<c:out value="${idUser}"/>">Annonces</a></li>
	     <li><a href="FormModifUtilisateur?id=<c:out value="${idUser}"/>&modif=mesinfos">Compte</a></li>
        <li><a href="index.jsp"> <img src="assets/logout.png" alt="" id="logout"> </a></li>
      </ul>
    </div>
  </header>

 <!--  FORMULAIRE ---------------------------->
  <div class="" id="containeformulaire">
    <form action="UpdateAnnonce" method="post">
		<input type="hidden" name="id_annonce" value="<c:out value="${annonce.idAnnonce}"/>">
		<input type="hidden" name="date_annonce" value="<c:out value="${annonce.dateAnnonce}"/>">
		<input type="hidden" name="id_user" value="<c:out value="${idUser}"/>">

        <h1>Ajouter une annonce</h1>
        
        <div id="case">


      	<input type="text" name="titre_annonce" placeholder="Titre annonce" class="champ" value="<c:out value="${annonce.titreAnnonce}"/>"/>
      	<input type="text" name="titre_livre" placeholder="Titre Livre" class="champ" value="<c:out value="${annonce.titreLivre}"/>"/>
        <div class="">
          <label for="ListeSco"> Niveau Scolaire </label>
        	<select name="niv_scolaire" id="ListeSco" class="champ">
                <option value="primaire">Primaire</option>
                <option value="college">College</option>
                <option value="lycee">Lycee</option>
                <option value="univ">Universitaire</option>
            </select>
        </div>

          <input type="text" name="isbn" class="champ" placeholder="ISBN" value="<c:out value="${annonce.isbn}"/>"/>
          <input type="date" name="dateEdition" class="champ" placeholder="Date d'edition" value="<c:out value="${annonce.dateEdition}"/>"/>
          <input type="text" name="maisonEdition" class="champ" placeholder="Maison d'edition" value="<c:out value="${annonce.maisonEdition}"/>"/>
          <input type="number" name="prixUni" class="champ" placeholder="Prix Unitaire" value="<c:out value="${annonce.prixUni}"/>"/>
          <input type="number" name="quantite" class="champ" placeholder="quantit�" min="0" max="999999" value="<c:out value="${annonce.quantite}"/>"/>
          <input type="number" name="remise" class="champ" placeholder="Remise" min="0" max="100" value="<c:out value="${annonce.remise}"/>"/>

      	<input type="file" name="photo1" class="champ">
      	<input type="file" name="photo2" class="champ">
      	<input type="file" name="photo3" class="champ">
      </div>
        <button type="submit" id="boutonValide"><img src="assets/icones_modif.png" alt=""></button>

      </form>
  </div>
</body>
</html>