package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.services.MetierUser;

/**
 * Servlet implementation class AddUser
 */
public class AddUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("goCreate !!!");
		String nomLib = request.getParameter("nom_librairie");
		String nomUser = request.getParameter("nom_user");
		String prenomUser = request.getParameter("prenom_user");
		String email = request.getParameter("email");
		String tel = request.getParameter("num_tel");
		String login = request.getParameter("login");
		String mdp = request.getParameter("pwd");
		String mdp2 = request.getParameter("pwd2");
		String numVoie = request.getParameter("num_voie");
		String nomVoie = request.getParameter("nom_voie");
		String cp = request.getParameter("cpostal");
		String ville = request.getParameter("ville");
		MetierUser mu = new MetierUser();
		if(mdp.equals(mdp2)) {
			mu.addUser(nomLib,nomUser,prenomUser,email,tel,login,mdp,numVoie,nomVoie,cp,ville);
		}else {
			request.setAttribute("errorMessage", "le mot de passe et sa confirmation ne sont pas identique");
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
		dispatcher.forward(request, response);
		
	}

}
