package fr.afpa.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.services.MetierAnnonce;
import fr.afpa.services.MetierUser;

/**
 * Servlet implementation class UpdateUser
 */
public class UpdateUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println(request.getParameter("id"));
		int idUser = Integer.parseInt(request.getParameter("id"));
		String nomLib = request.getParameter("nom_librairie");
		String nom = request.getParameter("nom_user");
		String prenom = request.getParameter("prenom_user");
		String email = request.getParameter("email");
		String tel = request.getParameter("num_tel");
		int idCompte = Integer.parseInt(request.getParameter("idCompte"));
		String login = request.getParameter("login");
		String pwd = request.getParameter("pwd");
		String pwd2 = request.getParameter("pwd2");
		int idAdresse = Integer.parseInt(request.getParameter("idAdresse"));
		String numVoie = request.getParameter("num_voie");
		String nomVoie = request.getParameter("nom_voie");
		String cpostal = request.getParameter("cpostal");
		String ville = request.getParameter("ville");
		MetierUser mu = new MetierUser();
		MetierAnnonce ma = new MetierAnnonce();
		if(pwd.equals(pwd2)) {
			mu.modifUser(idUser, nomLib, nom, prenom, email, tel,idCompte,login,pwd,idAdresse,numVoie,nomVoie,cpostal,ville);
		}
		Utilisateur user = mu.rechercherUser(idUser); 
	
		request.setAttribute("user", user);
		ArrayList<Annonce> listAnnonce = ma.listerAnnoncesUser(user);
		request.setAttribute("listAnnonce", listAnnonce);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/MenuUtilisateur.jsp");
		dispatcher.forward(request, response);
	}

}
