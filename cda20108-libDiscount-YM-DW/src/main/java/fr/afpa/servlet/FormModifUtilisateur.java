package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Utilisateur;
import fr.afpa.services.MetierUser;

/**
 * Servlet implementation class FormModifUtilisateur
 */
public class FormModifUtilisateur extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormModifUtilisateur() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		String modif = request.getParameter("modif");
		MetierUser mu = new MetierUser();
		Utilisateur user = mu.rechercherUser(id);
		System.out.println(user.getNom());
		request.setAttribute("user",user);
		RequestDispatcher dispatcher = null;
		switch (modif) {
		case "mesinfos":
			dispatcher = request.getRequestDispatcher("/WEB-INF/FormulaireModifMesInfos.jsp");
			dispatcher.forward(request, response);
			break;
		case "mdp":
			dispatcher = request.getRequestDispatcher("/WEB-INF/FormulaireModifMdp.jsp");
			dispatcher.forward(request, response);
			break;

		default:
			dispatcher = request.getRequestDispatcher("/WEB-INF/MenuUtilisateur.jsp");
			dispatcher.forward(request, response);
			break;
		}
		
	}

}
