package fr.afpa.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.services.MetierAnnonce;
import fr.afpa.services.MetierUser;

/**
 * Servlet implementation class DeleteAnnonce
 */
public class DeleteAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteAnnonce() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idUser = Integer.parseInt(request.getParameter("iduser"));
		int idAnnonce = Integer.parseInt(request.getParameter("idannonce"));
		MetierAnnonce ma = new MetierAnnonce();
		Annonce annonce = ma.rechercherAnnonce(idAnnonce);
		if(annonce != null) {
			ma.deleteAnnonce(annonce);
		}
		MetierUser mu = new MetierUser();
		Utilisateur user = mu.rechercherUser(idUser);
		ArrayList<Annonce> listAnnonce = ma.listerAnnoncesUser(user);
		request.setAttribute("listAnnonce", listAnnonce);
		request.setAttribute("user", user);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/MenuUtilisateur.jsp");
		dispatcher.forward(request, response);
	}


}
