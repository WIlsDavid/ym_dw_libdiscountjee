package fr.afpa.servlet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import fr.afpa.beans.Annonce;

/**
 * Servlet implementation class TestImageField
 */
public class TestImageField extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String path;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TestImageField() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/TestImageField.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ServletFileUpload sf = new ServletFileUpload(new DiskFileItemFactory());
		try {
			List<FileItem> multifiles = sf.parseRequest(request);
			Annonce annonce = new Annonce();
			int count = 1;
			int idUser = 0;
			LocalDate dateAnnonce = LocalDate.now();
			String name = null;
			for (FileItem item : multifiles) {
				File folder = new File(getServletContext().getRealPath("") + File.separator + "annonce");
				if (!folder.exists()) {
					folder.mkdir();
				}
				if (!item.isFormField()) {
					File image = new File(folder, "photo" + idUser + count+".jpg");
					item.write(image);
					System.out.println(image.getAbsolutePath());
					path = folder + File.separator + name;
					count++;
				} else {
					switch (item.getFieldName()) {
					case "id_user":
						idUser = Integer.parseInt(item.getString("UTF8"));
						System.out.println("id user = " + idUser);
						break;
					case "titre_annonce":
						String titreAnnonce = item.getString("UTF8");
						System.out.println("titre annonce = " + titreAnnonce);
						break;
					case "titre_livre":
						String titreLivre = item.getString("UTF8");
						System.out.println("titre livre = " + titreLivre);
						break;
					case "niv_scolaire":
						String nivScolaire = item.getString("UTF8");
						System.out.println("nivScolaire = " + nivScolaire);
						break;
					case "isbn":
						String isbn = item.getString("UTF8");
						System.out.println("isbn = " + isbn);
						break;
					case "dateEdition":
						LocalDate dateEdition = LocalDate.parse(item.getString("UTF8"));
						System.out.println("dateEdition = " + dateEdition);
						break;
					case "maisonEdition":
						String maisonEdition = item.getString("UTF8");
						System.out.println("maisonEdition = " + maisonEdition);
						break;
					case "prixUni":
						double prixUni = Double.parseDouble(item.getString("UTF8"));
						System.out.println("prixUni = " + prixUni);
						break;
					case "quantite":
						int quantite = Integer.parseInt(item.getString("UTF8"));
						System.out.println("quantite = " + quantite);
						break;
					case "remise":
						double remise = Double.parseDouble(item.getString("UTF8"));
						System.out.println("remise = " + remise);
						break;
					}
				}
			}
		} catch (FileUploadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/TestImageField.jsp");
		dispatcher.forward(request, response);
	}

}
