package fr.afpa.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.services.dao.DaoAnnonce;
import fr.afpa.services.dao.DaoUser;

/**
 * Servlet implementation class UtilisateurAdmin
 */
public class UtilisateurAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UtilisateurAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int idUser = Integer.parseInt(request.getParameter("id"));
		DaoUser DaoU = new DaoUser();
		Utilisateur user = DaoU.rechercherUser(idUser);
		request.setAttribute("user", user);
		
		DaoAnnonce DaoA = new DaoAnnonce(); 
		List<Annonce> list = DaoA.listerAnnonceParUser(user);
		request.setAttribute("ListeAnnonce", list);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/InfoUserAdmin.jsp");
		dispatcher.forward(request, response);
	}

	

}
