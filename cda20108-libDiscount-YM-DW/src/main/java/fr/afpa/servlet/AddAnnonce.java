package fr.afpa.servlet;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Image;
import fr.afpa.beans.Utilisateur;
import fr.afpa.services.MetierAnnonce;
import fr.afpa.services.MetierUser;
import fr.afpa.services.dao.DaoUser;

/**
 * Servlet implementation class AddAnnonce
 */
public class AddAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddAnnonce() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idUser = Integer.parseInt(request.getParameter("id"));
		request.setAttribute("idUser",idUser);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/FormAjoutAnnonce.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = null;
		int count = 1;
		int idUser = 0;
		String titreAnnonce = null;
		String titreLivre = null;
		String nivScolaire = null;
		String isbn = null;
		LocalDate dateEdition = null;
		String maisonEdition = null;
		double prixUni = 0;
		int quantite = 0;
		double remise = 0;
		LocalDate dateAnnonce = LocalDate.now();
		MetierUser mu =  new MetierUser();
	    MetierAnnonce ma = new MetierAnnonce();
	    Utilisateur user = null;
	    ArrayList<String> listImagePath = new ArrayList<String>();
		
		ServletFileUpload sf = new ServletFileUpload(new DiskFileItemFactory());
		try {
			List<FileItem> multifiles = sf.parseRequest(request);
			Annonce annonce = new Annonce();
			LocalDateTime datetimedata = LocalDateTime.now();
			long longTime = datetimedata.atOffset(ZoneOffset.UTC).toInstant().toEpochMilli();
			String name = null;
			for (FileItem item : multifiles) {
				File folder = new File(getServletContext().getRealPath("") + File.separator + "annonce");
				if (!folder.exists()) {
					folder.mkdir();
				}
				if (!item.isFormField()) {
					System.out.println(item.getName());
					String[] nameSplit = item.getName().split("\\.");
					if(nameSplit.length >= 2) {
						String imageEndPath = "photo_" + longTime + "_" + idUser + "_" + count+"."+nameSplit[nameSplit.length-1];
						File image = new File(folder, imageEndPath);
						item.write(image);
						System.out.println(image.getAbsolutePath());
						listImagePath.add(imageEndPath);
						count++;
					}
					
				} else {
					switch (item.getFieldName()) {
					case "id_user":
						idUser = Integer.parseInt(item.getString("UTF8"));
					    user = mu.rechercherUser(idUser);
					    request.setAttribute("user",user);
						break;
					case "titre_annonce":
						titreAnnonce = item.getString("UTF8");
						break;
					case "titre_livre":
						titreLivre = item.getString("UTF8");
						break;
					case "niv_scolaire":
						nivScolaire = item.getString("UTF8");
						break;
					case "isbn":
						isbn = item.getString("UTF8");
						break;
					case "dateEdition":
						dateEdition = LocalDate.parse(item.getString("UTF8"));
						break;
					case "maisonEdition":
						maisonEdition = item.getString("UTF8");
						break;
					case "prixUni":
						prixUni = Double.parseDouble(item.getString("UTF8"));
						break;
					case "quantite":
						quantite = Integer.parseInt(item.getString("UTF8"));
						break;
					case "remise":
						remise = Double.parseDouble(item.getString("UTF8"));
						break;
					}
				}
			}
		} catch (FileUploadException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	    if(user != null) {
	    	ma.addAnnonce(user,titreAnnonce,titreLivre,nivScolaire,isbn,dateEdition,maisonEdition,prixUni,quantite,remise,dateAnnonce,listImagePath);
	    	ArrayList<Annonce> listAnnonce = ma.listerAnnoncesUser(user);
			request.setAttribute("listAnnonce", listAnnonce);
	    	dispatcher = request.getRequestDispatcher("/WEB-INF/MenuUtilisateur.jsp");
			dispatcher.forward(request, response);
	    }else {
			dispatcher = request.getRequestDispatcher("/WEB-INF/FormAjoutAnnonce.jsp");
			dispatcher.forward(request, response);
		} 
	}
}
