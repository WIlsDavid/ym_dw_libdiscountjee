package fr.afpa.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.services.MetierAnnonce;
import fr.afpa.services.MetierUser;

/**
 * Servlet implementation class ConnexionUtilisateur
 */
public class ConnexionUtilisateur extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConnexionUtilisateur() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login = request.getParameter("login");
		System.out.println(login);
		String pwd = request.getParameter("pwd");
		MetierUser mu = new MetierUser();
		MetierAnnonce ma = new MetierAnnonce();
		Utilisateur user = mu.checkLogin(login,pwd);
		if(user != null) {
			request.setAttribute("user", user);
			ArrayList<Annonce> listAnnonce = ma.listerAnnoncesUser(user);
			request.setAttribute("listAnnonce", listAnnonce);
			System.out.println(user.getListAnnonce());
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/MenuUtilisateur.jsp");
			dispatcher.forward(request, response);
		}else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
			dispatcher.forward(request, response);
		}
	}

}
