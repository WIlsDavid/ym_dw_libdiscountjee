package fr.afpa.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.services.MetierAnnonce;
import fr.afpa.services.MetierUser;
import fr.afpa.services.dao.DaoAnnonce;

/**
 * Servlet implementation class DeleteAnnonceAdmin
 */
public class DeleteAnnonceAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteAnnonceAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int idAnnonce = Integer.parseInt(request.getParameter("idannonce"));
		MetierAnnonce ma = new MetierAnnonce();
		Annonce annonce = ma.rechercherAnnonce(idAnnonce);
		if(annonce != null) {
			ma.deleteAnnonce(annonce);
		}
		DaoAnnonce DaoA = new DaoAnnonce(); 
		List<Annonce> list = DaoA.listerAnnonce();
		request.setAttribute("ListeAnnonce", list);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/ListeAnnoncesAdmin.jsp");
		dispatcher.forward(request, response);
	}



}
