package fr.afpa.servlet;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.services.MetierAnnonce;
import fr.afpa.services.MetierUser;
import fr.afpa.services.dao.DaoUser;

/**
 * Servlet implementation class UpdateAnnonce
 */
public class UpdateAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateAnnonce() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idUser = Integer.parseInt(request.getParameter("iduser"));
		int idAnnonce = Integer.parseInt(request.getParameter("idannonce"));
		DaoUser DaoU = new DaoUser();
		Utilisateur user = DaoU.rechercherUser(idUser);
		MetierAnnonce ma = new MetierAnnonce();
		Annonce annonce = ma.rechercherAnnonce(idAnnonce);
		request.setAttribute("user", user);
		request.setAttribute("annonce",annonce);
		request.setAttribute("idUser",idUser);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/FormulaireModifAnnonce.jsp");
		dispatcher.forward(request, response);	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idUser = Integer.parseInt(request.getParameter("id_user"));
		int idAnnonce = Integer.parseInt(request.getParameter("id_annonce"));
		LocalDate dateAnnonce = LocalDate.parse(request.getParameter("date_annonce"));
		String titreAnnonce = request.getParameter("titre_annonce");
		String titreLivre = request.getParameter("titre_livre");
		String nivScolaire = request.getParameter("niv_scolaire");
		String isbn = request.getParameter("isbn");
		LocalDate dateEdition = LocalDate.parse(request.getParameter("dateEdition"));
		String maisonEdition = request.getParameter("maisonEdition");
		double prixUni = Double.parseDouble(request.getParameter("prixUni"));
		int quantite = Integer.parseInt(request.getParameter("quantite"));
		double remise = Double.parseDouble(request.getParameter("remise"));
		
		MetierUser mu =  new MetierUser();		
		MetierAnnonce ma = new MetierAnnonce();
		RequestDispatcher dispatcher = null;
		Utilisateur user = mu.rechercherUser(idUser);
		request.setAttribute("user", user);
		if (user != null) {
			ma.updateAnnonce(user, idAnnonce , titreAnnonce, titreLivre, nivScolaire, isbn, dateEdition, maisonEdition, prixUni, quantite, remise, dateAnnonce);
		}
		ArrayList<Annonce> listAnnonce = ma.listerAnnoncesUser(user);
		request.setAttribute("listAnnonce", listAnnonce);
		dispatcher = request.getRequestDispatcher("/WEB-INF/MenuUtilisateur.jsp");
		dispatcher.forward(request, response);
	}

}
