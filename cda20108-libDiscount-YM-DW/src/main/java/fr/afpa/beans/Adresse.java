package fr.afpa.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Entity

/**
 * 
 
 *
 */
public class Adresse {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idAdresse;

	@NonNull
	@Column(name = "num�ro_rue")
	private String numRue;

	@NonNull
	@Column(name = "nom_rue")
	private String nomRue;

	@NonNull
	@Column(name = "code_postal")
	private String codeP;

	@NonNull
	private String ville;
}
