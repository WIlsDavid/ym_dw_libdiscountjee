package fr.afpa.beans;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@RequiredArgsConstructor
@Entity
@NoArgsConstructor

public class Image {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idImage;
	@NonNull
	private String lienImage;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idAnnonce")
	private Annonce annonce;

}
