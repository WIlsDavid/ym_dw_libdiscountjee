package fr.afpa.beans;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@NamedQueries({
	@NamedQuery(name ="FindAnnonceByVille", query="FROM Annonce a where a.user.adresse.ville =:value"),
	@NamedQuery(name = "FindAnnonceByIsbn", query="FROM Annonce a where a.isbn =:value"),
	@NamedQuery(name = "FindAnnonceByMotcle", query="FROM Annonce a where a.titreAnnonce LIKE :value or a.titreLivre LIKE :value2"),
	@NamedQuery(name = "FindAnnonceByidUser", query="FROM Annonce a where a.user.idUser =:value"),
	@NamedQuery(name = "findAnnonceByIdAnnonce", query="FROM Annonce a where a.idAnnonce =:value"),
	
})


public class Annonce {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idAnnonce;
	@NonNull
	private String titreAnnonce;
	@NonNull
	private String titreLivre;
	@NonNull
	private String niveauScolaire;
	@NonNull
	private String isbn;
	@NonNull
	private LocalDate dateEdition;
	@NonNull
	private String maisonEdition;
	@NonNull
	private double prixUni;
	@NonNull
	private int quantite;
	@NonNull
	private double remise;
	@NonNull
	private LocalDate dateAnnonce;
	
	@ManyToOne(cascade = CascadeType.DETACH )
	@JoinColumn(name = "id_user")
	private Utilisateur user;
	
//	@OneToMany(mappedBy = "annonce", fetch = FetchType.EAGER)
//	private List<Image> listImage;
	
	private String image1;
	private String image2;
	private String image3;
	
}
