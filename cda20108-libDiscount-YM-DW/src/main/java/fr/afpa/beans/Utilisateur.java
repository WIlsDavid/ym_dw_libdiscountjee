package fr.afpa.beans;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@NamedQueries({
		@NamedQuery(name = "findById", query = "from Utilisateur u where u.idUser = :value"),
		@NamedQuery(name = "findByEmailOrLogin", query = "from Utilisateur u where u.mail =:value or u.compte.login =:value2")
})




public class Utilisateur {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_user")
	private int idUser;

	@NonNull
	private String nom;

	@NonNull
	private String prenom;

	@NonNull
	private String mail;

	@NonNull
	private String nomLib;

	@NonNull
	private String tel;

	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "fk_adresse")
	private Adresse adresse;

	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "fk_compte")
	private Compte compte;
	
	@OneToMany(mappedBy = "user" , fetch = FetchType.EAGER)
	private List<Annonce> listAnnonce;

}
