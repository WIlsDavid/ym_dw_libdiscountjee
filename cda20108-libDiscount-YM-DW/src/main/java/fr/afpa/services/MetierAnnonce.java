package fr.afpa.services;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.services.dao.DaoAnnonce;

/**
 * 
 * @author david
 *
 */
public class MetierAnnonce {


	/**
	 * Ajouter une annonce avec les images
	 * @param user
	 * @param titreAnnonce
	 * @param titreLivre
	 * @param nivScolaire
	 * @param isbn
	 * @param dateEdition
	 * @param maisonEdition
	 * @param prixUni
	 * @param quantite
	 * @param remise
	 * @param dateAnnonce
	 * @param listImagePath
	 */
	public void addAnnonce(Utilisateur user, String titreAnnonce, String titreLivre, String nivScolaire, String isbn,
			LocalDate dateEdition, String maisonEdition, double prixUni, int quantite, double remise,
			LocalDate dateAnnonce, ArrayList<String> listImagePath) {
		Annonce annonce = new Annonce(titreAnnonce, titreLivre, nivScolaire, isbn, dateEdition, maisonEdition, prixUni, quantite, remise, dateAnnonce);
		int count = 0;
		for (String imagePath : listImagePath) {
			++count;
			switch (count) {
			case 1:
				annonce.setImage1(imagePath);
				break;
			case 2:
				annonce.setImage2(imagePath);
				break;
			case 3:
				annonce.setImage3(imagePath);
				break;
			}
		}
		DaoAnnonce daoA = new DaoAnnonce();
		daoA.ajouterAnnonce(annonce, user);
	}

	/**
	 * rechercher une annonce � partir de l'id de l'annonce
	 * @param idAnnonce
	 * @return
	 */
	public Annonce rechercherAnnonce(int idAnnonce) {
		DaoAnnonce daoA = new DaoAnnonce();
		return daoA.rechercherAnnonce(idAnnonce) ;
	}

	/**
	 * Mise � jour de l'annonce
	 * @param user
	 * @param idAnnonce
	 * @param titreAnnonce
	 * @param titreLivre
	 * @param nivScolaire
	 * @param isbn
	 * @param dateEdition
	 * @param maisonEdition
	 * @param prixUni
	 * @param quantite
	 * @param remise
	 * @param dateAnnonce
	 */
	public void updateAnnonce(Utilisateur user, int idAnnonce, String titreAnnonce, String titreLivre,
			String nivScolaire, String isbn, LocalDate dateEdition, String maisonEdition, double prixUni, int quantite,
			double remise, LocalDate dateAnnonce) {
		Annonce annonce = new Annonce(titreAnnonce, titreLivre, nivScolaire, isbn, dateEdition, maisonEdition, prixUni, quantite, remise, dateAnnonce);
		annonce.setIdAnnonce(idAnnonce);
		DaoAnnonce daoA = new DaoAnnonce();
		daoA.modifierAnnonce(annonce, user);
		
		
	}
	
	/**
	 * liste toutes les annonces
	 * @return liste des annonces
	 */
	public List<Annonce> ListAnnonce(){
		DaoAnnonce daoA = new DaoAnnonce();
		ArrayList<Annonce> listAnnonce = (ArrayList<Annonce>) daoA.listerAnnonce();
		return listAnnonce;
		
	}

	/**
	 * Supprimer l'annonce
	 * @param annonce
	 */
	public void deleteAnnonce(Annonce annonce) {
		DaoAnnonce daoA = new DaoAnnonce();
		daoA.supprimeAnnonce(annonce);
	}

	public ArrayList<Annonce> listerAnnoncesUser(Utilisateur user) {
		DaoAnnonce daoA = new DaoAnnonce();
		ArrayList<Annonce> listAnnonce = (ArrayList<Annonce>) daoA.listerAnnonceParUser(user);
		return listAnnonce;
	}

	

}
