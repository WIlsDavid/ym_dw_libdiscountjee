package fr.afpa.services.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;
import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.utils.HibernateUtils;

public class DaoAnnonce {
	private static Session session;
	

	/**
	 * Ajoute une annonce
	 * @param annonce
	 * @param listImagePath 
	 */
	public void ajouterAnnonce(Annonce annonce , Utilisateur user) {
		session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		annonce.setUser(user);
		List<Annonce> listAnnonce = (List<Annonce>) user.getListAnnonce();
		listAnnonce.add(annonce);
		user.setListAnnonce(listAnnonce);
		session.save(annonce);
		session.update(user);
		tx.commit();
		session.close();
	}

	/**
	 * Modifie une annonce
	 * @param annonce
	 * @param user 
	 */
	public void modifierAnnonce(Annonce annonce, Utilisateur user) {
		session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		annonce.setUser(user);
		session.update(annonce);	
		session.update(user);
		tx.commit();
		session.close();
	}

	
	/**
	 * Supprime une annonce
	 * @param annonce
	 */
	public void supprimeAnnonce(Annonce annonce) {
		session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		session.delete(annonce);
		tx.commit();
		session.close();
	}

	
	/**
	 * Rechercher une annonce
	 * @param idAnnonce
	 * @return annonce
	 */
	public Annonce rechercherAnnonce(int idAnnonce) {	
		session = HibernateUtils.getSession();
		Query req = session.createNamedQuery("findAnnonceByIdAnnonce");
		req.setParameter("value", idAnnonce);
		List<Annonce> listAnnonce = (List<Annonce>)req.getResultList();
		session.close();
		for (Annonce annonce : listAnnonce) {
			return annonce;
		}
		return null;
	}

	/**
	 * Liste toutes les annonces
	 * 
	 * @return list<Annonce> : toutes les annonces
	 */
	public List<Annonce> listerAnnonce() {
		session = HibernateUtils.getSession();
		Query req = session.createQuery("from Annonce");
		ArrayList<Annonce> listAnnonce = (ArrayList<Annonce>) req.getResultList();
		return listAnnonce;
	}
	

	/**
	 * Liste d'annonces de l'utilisateur
	 * 
	 * @param idUser
	 * @return list<Annonce>
	 */
	public ArrayList<Annonce> listerAnnonceParUser(Utilisateur user){	
		session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("FindAnnonceByidUser");
		req.setParameter("value", user.getIdUser());
		ArrayList<Annonce> listAnnonce = (ArrayList<Annonce>) req.getResultList();
		return listAnnonce;
	}

	/**
	 * Recherche d'annonce par mot cl�
	 * 
	 * @param str
	 * @return list<Annonce>
	 */
	public List<Annonce> listeAnnonceParMotcle(String str) {

		session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("FindAnnonceByMotcle");
		req.setParameter("value", "'%"+str +"%'");
		ArrayList<Annonce> listAnnonce = (ArrayList<Annonce>) req.getResultList();
		return listAnnonce;
	}

	/**
	 * Recherche d'annonce avec le isbn
	 * 
	 * @param isbn
	 * @return list<Annonce>
	 */
	public List<Annonce> listeAnnonceParISBN(String isbn) {

		session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("FindAnnonceByIsbn");
		req.setParameter("value", isbn);
		ArrayList<Annonce> listAnnonce = (ArrayList<Annonce>) req.getResultList();
		return listAnnonce;

	}

	/**
	 * recherche d'annonce par ville
	 * 
	 * @param ville
	 * @return list<Annonce>
	 */
	public List<Annonce> listeAnnonceParVille(String ville) {
		session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("FindAnnonceByVille");
		req.setParameter("value", ville);
		ArrayList<Annonce> listAnnonce = (ArrayList<Annonce>) req.getResultList();
		return listAnnonce;
	}


	

}
