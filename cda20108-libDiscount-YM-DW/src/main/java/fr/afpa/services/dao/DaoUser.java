package fr.afpa.services.dao;

import java.util.ArrayList;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Compte;
import fr.afpa.beans.Utilisateur;
import fr.afpa.utils.HibernateUtils;

public class DaoUser {

	private Session session = null;
	
	
	/**
	 * Authentification du compte de l'utilisateur, recherche de son compte avec son email ou son login
	 * @param login
	 * @param mdp
	 * @return Utilisateur
	 */
	public Utilisateur authUser(String login, String mdp) {
		session = HibernateUtils.getSession();
		Query q = session.getNamedQuery("findByEmailOrLogin");
		System.out.println("login : "  + login);
		q.setParameter("value", login);
		q.setParameter("value2", login);

		ArrayList<Utilisateur> listUser = (ArrayList<Utilisateur>) q.getResultList();
		session.close();
		for (Utilisateur user : listUser) {
			System.out.println("Trouv� !!!");
			return user;
		}
		System.out.println("Pas Trouv� !!!");
		return null;
	}

	/**
	 * Ajout de l'utilisateur
	 * @param user
	 * @param compte
	 * @param adresse
	 */
	public void ajoutUser(Utilisateur user, Compte compte, Adresse adresse) {
		session = HibernateUtils.getSession();

		user.setAdresse(adresse);
		user.setCompte(compte);

		Transaction tx = session.beginTransaction();
		session.save(user);

		tx.commit();
		session.close();

	}

	/**
	 * Recherche de l'utilisateur avec son id
	 * @param idUser
	 * @return
	 */
	public Utilisateur rechercherUser(int idUser) {

		session = HibernateUtils.getSession();

		Query q = session.getNamedQuery("findById");

		q.setParameter("value", idUser);

		Utilisateur user = (Utilisateur) q.getSingleResult();
		session.close();
		if (user != null && user.getIdUser() > 0) {
			System.out.println(user);	
			return user;
		}
		return null;
	}

	public void modifierMdpUser(Compte compte) {

		session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		session.update(compte);
		tx.commit();
		session.close();
	}

	/**
	 * Supprimer un utilisateur
	 * @param idUser
	 */
	public void supprimerUser(int idUser) {

		session = HibernateUtils.getSession();

		Transaction tx = session.beginTransaction();

		Query req = session.getNamedQuery("findById");
		req.setParameter("parametre", idUser);

		ArrayList<Utilisateur> listeUser = (ArrayList<Utilisateur>) req.getResultList();

		for (Utilisateur users : listeUser) {

			session.delete(users);

			System.out.println("Votre compte a bien �t� supprimer");

		}
		tx.commit();
		session.close();

	}

	public void modifierUser(Utilisateur user, Compte compte, Adresse adresse) {
		session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		user.setAdresse(adresse);
		user.setCompte(compte);
		session.update(adresse);
		session.update(compte);
		session.update(user);
		tx.commit();
		session.close();
		
	}

	public ArrayList<Utilisateur> listerUsers() {
		session = HibernateUtils.getSession();

		Transaction tx = session.beginTransaction();

		Query req = session.createQuery("From Utilisateur");

		ArrayList<Utilisateur> listeUser = (ArrayList<Utilisateur>) req.getResultList();
		return listeUser;
	}
	
}
