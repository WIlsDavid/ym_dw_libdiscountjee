package fr.afpa.services;

import java.util.ArrayList;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Compte;
import fr.afpa.beans.Utilisateur;
import fr.afpa.services.dao.DaoUser;

/**
 * 
 * @author david
 */
public class MetierUser {

	/**
	 * Verifie le login
	 * @param login
	 * @param pwd
	 * @return
	 */
	public Utilisateur checkLogin(String login, String pwd) {
		DaoUser daoU = new DaoUser();
		return daoU.authUser(login, pwd);
	}

	/**
	 * ajoute l'utilisateur
	 * @param nomLib
	 * @param nomUser
	 * @param prenomUser
	 * @param email
	 * @param tel
	 * @param login
	 * @param mdp
	 * @param numVoie
	 * @param nomVoie
	 * @param cp
	 * @param ville
	 */
	public void addUser(String nomLib, String nomUser, String prenomUser, String email, String tel, String login , String mdp,
			String numVoie, String nomVoie, String cp, String ville ) {
		Utilisateur user = new Utilisateur(nomUser, prenomUser, email, nomLib, tel);
		Compte compte = new Compte(login,mdp);
		Adresse adresse = new Adresse(numVoie,nomVoie,cp,ville);
		DaoUser daoU = new DaoUser();
		daoU.ajoutUser(user, compte, adresse);
		
	}

	/**
	 * rechercher utilisateur
	 * @param id
	 * @return utilisateur
	 */
	public Utilisateur rechercherUser(int id) {
		DaoUser daoU = new DaoUser();
		return daoU.rechercherUser(id);
	}

	/**
	 * 
	 * @param idCompte
	 * @param login
	 * @param pwd
	 */
	public void modifMdp(int idCompte, String login, String pwd) {
		Compte compte = new Compte(login,pwd);
		compte.setIdCompte(idCompte);
		DaoUser daoU = new DaoUser();
		daoU.modifierMdpUser(compte);
	}

	/**
	 * modifier l'Utilisateur
	 * @param idUser
	 * @param nomLib
	 * @param nom
	 * @param prenom
	 * @param email
	 * @param tel
	 * @param idCompte
	 * @param login
	 * @param pwd
	 * @param idAdresse
	 * @param numVoie
	 * @param nomVoie
	 * @param cpostal
	 * @param ville
	 */
	public void modifUser(int idUser, String nomLib, String nom, String prenom, String email, String tel, int idCompte,
			String login, String pwd, int idAdresse, String numVoie, String nomVoie, String cpostal, String ville) {
		Utilisateur user = new Utilisateur();
		user.setIdUser(idUser);
		user.setNom(nom);
		user.setPrenom(prenom);
		user.setMail(email);
		user.setNomLib(nomLib);
		user.setTel(tel);
		Adresse adresse = new Adresse();
		adresse.setIdAdresse(idAdresse);
		adresse.setNumRue(numVoie);
		adresse.setNomRue(nomVoie);
		adresse.setCodeP(cpostal);
		adresse.setVille(ville);
		Compte compte = new Compte();
		compte.setIdCompte(idCompte);
		compte.setLogin(login);
		compte.setMdp(pwd);
		DaoUser daoU = new DaoUser();
		daoU.modifierUser(user,compte,adresse);
		
	}

	/**
	 * Liste des Utilisateurs pour l'admin
	 * @return
	 */
	public ArrayList<Utilisateur> listerUsers() {
		DaoUser daoU = new DaoUser();
		
		return daoU.listerUsers();
	}
	
	

}
