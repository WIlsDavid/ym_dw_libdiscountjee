
CREATE TABLE public.adresse (
	idadresse int4 NOT NULL,
	code_postal varchar(255) NULL,
	nom_rue varchar(255) NULL,
	num�ro_rue varchar(255) NULL,
	ville varchar(255) NULL,
	CONSTRAINT adresse_pkey PRIMARY KEY (idadresse)
);

CREATE TABLE public.compte (
	id_compte int4 NOT NULL,
	login varchar(255) NULL,
	mdp varchar(255) NULL,
	CONSTRAINT compte_pkey PRIMARY KEY (id_compte)
);

CREATE TABLE public.utilisateur (
	id_user int4 NOT NULL,
	mail varchar(255) NULL,
	nom varchar(255) NULL,
	nomlib varchar(255) NULL,
	prenom varchar(255) NULL,
	tel varchar(255) NULL,
	fk_adresse int4 NULL,
	fk_compte int4 NULL,
	CONSTRAINT utilisateur_pkey PRIMARY KEY (id_user)
);


-- public.utilisateur foreign keys

ALTER TABLE public.utilisateur ADD CONSTRAINT fkp3fttgy8ocb1o4jxpaaejih94 FOREIGN KEY (fk_compte) REFERENCES compte(id_compte);
ALTER TABLE public.utilisateur ADD CONSTRAINT fksbv69tq3epv8ql5ya5jbohbkj FOREIGN KEY (fk_adresse) REFERENCES adresse(idadresse);



CREATE TABLE public.annonce (
	idannonce int4 NOT NULL,
	dateannonce date NULL,
	dateedition date NULL,
	isbn varchar(255) NULL,
	maisonedition varchar(255) NULL,
	prixuni float8 NOT NULL,
	quantite int4 NOT NULL,
	remise float8 NOT NULL,
	titre varchar(255) NULL,
	id_user int4 NULL,
	niveauscolaire varchar(255) NULL,
	titreannonce varchar(255) NULL,
	titrelivre varchar(255) NULL,
	CONSTRAINT annonce_pkey PRIMARY KEY (idannonce)
);


-- public.annonce foreign keys

ALTER TABLE public.annonce ADD CONSTRAINT fkja95ieqfbprsafd5qphaqnh1o FOREIGN KEY (id_user) REFERENCES utilisateur(id_user);

CREATE TABLE public.archive (
	idannonce int4 NOT NULL,
	dateannonce date NULL,
	dateedition date NULL,
	isbn varchar(255) NULL,
	maisonedition varchar(255) NULL,
	prixuni float8 NOT NULL,
	quantite int4 NOT NULL,
	remise float8 NOT NULL,
	titre varchar(255) NULL,
	id_user int4 NULL,
	niveauscolaire varchar(255) NULL,
	titreannonce varchar(255) NULL,
	titrelivre varchar(255) NULL
);

CREATE TRIGGER archiver_annonce
BEFORE DELETE
ON public.annonce
FOR EACH ROW
EXECUTE PROCEDURE public.archivage();

CREATE FUNCTION public.archivage()
RETURNS trigger
LANGUAGE 'plpgsql'
AS $$
declare
rec record;
begin
	for rec in (select * from annonce) loop
		if rec.idannonce = old.idannonce then
			insert into archive ( 	idannonce,
									dateannonce,
									dateedition,
									isbn,
									maisonedition,
									prixuni,
									quantite,
									remise,
									titre,
									id_user,
									niveauscolaire,
									titreannonce,
									titrelivre
								          )
						values( 	old.idannonce,
									old.dateannonce,
									old.dateedition,
									old.isbn,
									old.maisonedition,
									old.prixuni,
									old.quantite,
									old.remise,
									old.titre,
									old.id_user,
									old.niveauscolaire,
									old.titreannonce,
									old.titrelivre
								);
		end if;
	end loop;
	RETURN rec;
	END;
	$$;
	

DROP TRIGGER archiver_annonce ON annonce;